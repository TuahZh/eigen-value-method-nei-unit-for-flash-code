# Eigen value method NEI unit for FLASH code

A faster NEI calculation unit for the FLASH code

## Directories description

-source/physics/sourceTerms/Ionize/IonizeEigen:

Eigen value method for NEI itself.

-simulations:

Some examples for the use of this unit.

-source/Driver:

The time step used for radiative cooling.
	
> other directories are necessary only if some parameters need to be comstumized.