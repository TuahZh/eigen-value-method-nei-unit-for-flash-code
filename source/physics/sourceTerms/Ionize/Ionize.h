#if 0
Maximum number of elements
#endif
#define ION_NELEM 12

#if 0
Maximum number of ionization states
#endif
#define ION_NIMAX 30

#if 0
Number of temperatures in the Summers table
#endif
#define ION_NTEMP 134

#if 0
Number of rows (or columns? I forgot) in eigen related data files
#endif
#define ION_NEIGEN 2001

#if 0
Number of temperatures in radiative energy file Power something
#endif
#define ION_NPOWER 51

#if 0
  Real maximum number of elements (should be merged with NELEM)
#endif
#define ION_NELEMP 28
