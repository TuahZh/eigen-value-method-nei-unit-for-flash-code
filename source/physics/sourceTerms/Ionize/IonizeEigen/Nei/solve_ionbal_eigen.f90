!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! File Name: solve_ionbal_eigen.f90
! Author: ZHANG, GaoYuan
! mail: zgy0106@gmail.com
! Created Time: Fri Mar 10 14:01:32 2017
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!description
!!
!!  NAME
!!
!!   solve_ionbal_eigen
!!
!!  SYNOPSIS
!!
!!   solve_ionbal_eigen(integer(IN)  :: inel, Z
!!                real(IN)  :: Te,
!!                real(IN), optional :: init_pop(Z+1),
!!                real(IN), optional :: tau
!!                real(IN), optional  :: Te_init
!!                real(OUT)    :: out_pop(Z+1))
!!
!!  DESCRIPTION
!!
!!   Computes the ions non-equilibrium ionization or equilibrium ionization
!!
!!  ARGUMENTS
!!
!!   INPUTS
!!           inel  -  INTEGER      Element I.D., may shift if there are elements not used
!!                               1,  2, 3, 4, 5,  6,  7,  8, 9,  10, 11, 12
!!                               he, c, n, o, ne, mg, si, s, ar, ca, fe, ni
!!           Z    -  INTEGER      Atomic No. of the Element (necessary for declaration
!!                                of some arrays)
!!                               2,  6, 7, 8, 10, 12, 14, 16,18, 20, 26, 28
!!                               he, c, n, o, ne, mg, si, s, ar, ca, fe, ni
!!           Te   -  REAL         Temperature of the final state;
!!                                Or the temperature for the equilibrium
!!           init_pop - REAL(Z+1) Input population fraction fo the current element,
!!                                if not given, a initial population will be calculated
!!                                using the temperature of Te_init
!!           tau  -  REAL         Time scale for NEI in unit of cm^{-3} s;
!!                                if not provided, an equilibrium result using Te will
!!                                be generated
!!           Te_init - REAL       Initial temperature
!!
!!   OUTPUTS
!!           out_pop - REAL(Z+1)  Out put population fraction of the current element
!!    [When only 3 input arguments are present, equilibrium population will be obtained]
!!
!!***

Subroutine solve_ionbal_eigen(inel, Z, Te, init_pop, tau, Te_init, out_pop)
  use Ionize_data, only : feqb => ion_feqb, vl => ion_vl, vr => ion_vr, &
       eig => ion_eig, &
       nsize, teind, telist
  implicit none

  integer, intent(in) :: inel, Z
  real, intent(in) :: Te
  real, dimension(Z+1), optional, intent(in) :: init_pop
  real, optional, intent(in) :: tau, Te_init
  real, dimension(Z+1), intent(out) :: out_pop
  real, dimension(Z+1) :: init_popr
  logical :: do_equilib = .False., do_nei=.True.
  real :: te_equilib, Tdiff, factorlow, factorhigh, delt
  integer :: i, j, itemin, itemin2, Tindex
  integer, dimension(2) :: ite
!  integer, parameter :: nsize = 1251
!  integer, parameter :: Z = 3
!  real, dimension(nsize) :: teind = (/(i, i=0, nsize-1, 1)/)
!  real, dimension(nsize) :: telist = 0.
  logical, dimension(nsize) :: temask=.True.
  real, dimension(Z+1) :: equilib, frac
  real, dimension(Z,Z) :: lefteigenvec, righteigenvec
  real, dimension(Z,1) :: work, fspectmp, worktmp
  real, dimension(1,Z) :: frac_tmp

  if (.not. present(tau)) then
 !   tau=-1
     do_equilib = .True.
     te_equilib = Te
     do_nei = .False.
  else
     do_nei = .True.
     if(.not. present(init_pop)) then
        if (.not. present(Te_init)) then
           print *, "Warning: neither 'init_pop' or 'Te_init' set, assuming ionization from neutral"
           do_equilib = .False.
           init_popr = 0.
           init_popr(1) = 1.
        else
           do_equilib = .True.
           Te_equilib = Te_init
        end if
     else
        do_equilib =  .False.
        init_popr = init_pop
     end if
  end if

  !!! ==========================
  ! open fits or some file for eigen matrix
  !!! ==========================

!  call readtable(Z, feqb, vl, vr, eig, nsize)

  out_pop = 1.

!  telist = teind/(nsize-1.)*(9.-4.)+4.
!  telist = 10**telist

!  print *, telist(1), telist(2), telist(1000)
  if (do_equilib) then
     itemin = minloc(abs(telist-Te_equilib), dim=1)
!     itemin = minloc((telist-Te_equilib)**2, dim=1)
     temask(itemin)=.False.
     itemin2 = minloc(abs(telist-Te_equilib),dim=1,mask=temask)
!     itemin2 = minloc((telist-Te_equilib)**2,dim=1,mask=temask)
     ite(1) = min(itemin, itemin2)
     ite(2) = max(itemin, itemin2)
     Tdiff = telist(ite(2))-telist(ite(1))
!     Tdiff = abs(telist(itemin2)-telist(itemin)) ! only two possibiliteis >0 or =0
     if (Tdiff>0) then
        factorlow = (telist(ite(2))-Te_equilib)/Tdiff
        factorhigh = (Te_equilib-telist(ite(1)))/Tdiff
!        equilib = factorlow*feqb(:,ite(1))+factorhigh*feqb(:,ite(2))
        equilib = factorlow*feqb(inel,:Z+1,ite(1))+factorhigh*feqb(inel,:Z+1,ite(2))
     else
        equilib = feqb(inel,:Z+1,ite(1))
!        equilib = feqb(nel,:Z+1,itemin)
     end if
  end if

  if(do_nei) then
     if(do_equilib) then
        init_popr = equilib
     end if

     ! renormalize
     init_popr = init_popr/sum(init_popr)
!     print *, sum(init_popr)
     Tindex = minloc(abs(telist-Te), dim=1)

     do i=1,Z
        do j=1,Z
           lefteigenvec(i,j) = vl(inel,(i-1)*Z+j, Tindex)
           righteigenvec(i,j) = vr(inel,(i-1)*Z+j, Tindex)
        end do
     end do

!     delt = 1.0/(size(telist)-1.)
     work(:,1) = init_popr(2:)-feqb(inel,2:Z+1,Tindex)
!     print *, lefteigenvec
!     print *, work
     fspectmp = (matmul(lefteigenvec, work))
!     fspectmp = (matmul(lefteigenvec, work))
     !! MAY USE SOME LIBRARY to do it with a higher performance

!     print *, fspectmp
     delt = 1.0

     do i=1,Z
        worktmp(i,1)=fspectmp(i,1)*exp(eig(inel, i, Tindex)*delt*tau)
     end do

     frac=0.
     frac_tmp = matmul(transpose(worktmp),(righteigenvec))
!     frac_tmp = matmul(transpose(worktmp),(righteigenvec))
!     do i=1,Z
!        do j=1,Z
!           frac(i+1)=frac(i+1)+worktmp(j,1)*righteigenvec(j,i)
!        end do
!        if (frac_tmp(1,i) .ne. frac(i+1)) then
!           print *, "frac_tmp is different"
!        end if
!        print *, frac_tmp(1,i), frac(i+1)
!        frac(i+1)=frac(i+1)+feqb(inel,i+1,Tindex)
!     end do
!     frac(2:) = frac_tmp(1,:)
     do i = 1,Z
        frac(i+1)=frac_tmp(1,i)+feqb(inel,i+1,Tindex)
     end do

     where (frac<0.) frac=0.D0

     if (sum(frac)<1.) then
        frac(1) = 1.0-sum(frac)
     end if

  end if

  if (present(tau)) then
     out_pop = frac
!     print *, sum(frac)
  else
     out_pop = equilib
  end if

!  call closetable

  return

End subroutine solve_ionbal_eigen
!
