!!****if* source/physics/sourceTerms/Ionize/IonizeMain/Ionize_equil
!!
!!  NAME
!!
!!   Ionize_equil
!!
!!  SYNOPSIS
!!
!!   Ionize_equil(real(INOUT)  :: tx, 
!!                integer(IN)  :: inel, 
!!                integer(IN) :: nion, 
!!                real(OUT)    :: delem(ION_NIMAX))
!!
!!  DESCRIPTION
!!
!!   Computes the ions density under equilibrium ionization.
!!
!!  ARGUMENTS
!!
!!   INPUTS
!!           tx   -  REAL         Extrapolated temperature
!!           inel  -  INTEGER      Element I.D., may shift
!!                               1,  2, 3, 4, 5,  6,  7,  8, 9,  10, 11, 12
!!                               he, c, n, o, ne, mg, si, s, ar, ca, fe, ni
!!
!!   OUTPUTS
!!           nion  -   INTEGER     Number of ionization states
!!           delem -   REAL        Population fraction
!!    [tx may be modified on output to force it into the allowed temperature range]
!!    [use Eigen vector method by Gao-Yuan, May 17 2017]
!!***

subroutine Ionize_equil(tx,inel,nw,delem)
!  use ion_interface, ONLY : ion_intCoeff, solve_ionbal_eigen
  use ion_interface, ONLY : solve_ionbal_eigen
  use Ionize_data, ONLY : nion12 => ion_nion12
  use Simulation_speciesData, ONLY : sim_specNumElect
  !
  !..declare
  implicit none
#include "Ionize.h"
  real, intent(INOUT) :: tx
  integer, intent(IN) :: inel
!  integer, intent(OUT) :: nion
  integer, intent(IN) :: nw
  real, dimension(ION_NIMAX), intent(OUT) :: delem
!  real, dimension(0:ION_NIMAX) :: cz
!  real, dimension(ION_NIMAX) :: al
!  integer :: i,j,nw
  integer :: i,j
!  real, allocatable :: out3(:)
  real, dimension(ION_NIMAX) :: out3
!  real :: cc
  !
  !---------------------------------------------------------------------
  !
  if (tx.ge.1.e4) then
!     call ion_intCoeff(nel,tx,nion)
!     nw = nion12(nel)
!     nion = nion12(nel)
!     nw = nion-1
!     cc = 1.
!     do i = nw,1,-1
!        cc = cz(i)/al(i+1)*cc+1.
!     end do
!     delem(1) = 1./cc
!     do i = 1,nw
!        delem(i+1) = cz(i)/al(i+1)*delem(i)
!     end do
!     nw = nion
!     allocate(out3(nw))
!     call solve_ionbal_equal(nel, nw-1, tx, out_pop=out3(1:nw))
     call solve_ionbal_eigen(inel, nw-1, tx, out_pop=out3(1:nw))
     do i = 1, nw
        delem(i) = out3(i)
     end do
  else
     call solve_ionbal_eigen(inel, nw-1, 1.e4, out_pop=out3(1:nw))
     do i = 1, nw
        delem(i) = out3(i)
     end do
!     delem(1) = 1.
!     do i = 2,ION_NIMAX
!        delem(i) = 0.
!     end do
  end if
  !
  !
  return
end subroutine Ionize_equil
